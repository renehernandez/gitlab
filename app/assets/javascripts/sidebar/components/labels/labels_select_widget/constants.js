export const SCOPED_LABEL_DELIMITER = '::';
export const DEBOUNCE_DROPDOWN_DELAY = 200;
export const DEFAULT_LABEL_COLOR = '#6699cc';

export const DropdownVariant = {
  Sidebar: 'sidebar',
  Standalone: 'standalone',
  Embedded: 'embedded',
};
